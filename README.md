Treść zadania: 
1. Utworzyć dwie klasy: Student oraz Nauczyciel.
2. Nauczyciel może mieć wiele studentów oraz student może mieć wiele nauczycieli.
3. Nauczyciel powinien mieć pola: imię, nazwisko, wiek, email oraz przedmiot.
4. Student powinien mieć pola: imię, nazwisko, wiek, email oraz kierunek.

Wymagania:
1. Obie klasy można tworzyć, usuwać i edytować.
2. Dane powinny być walidowane: poprawny email, imię dłuższe od dwóch liter, wiek > 18.
3. Powinna być możliwość wyświetlenia wszystkich studentów oraz wszystkich nauczycieli (dwa endpointy, możliwość stronicowania i sortowania).
4. Dane można filtrować: wyszukać wszystkich studentów danego nauczyciela i odwrotnie.
5. Studentów oraz nauczycieli można wyszukiwać po imieniu i nazwisku.
