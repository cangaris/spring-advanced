package pl.cansoft.work.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import pl.cansoft.work.dtos.StudentDto;
import pl.cansoft.work.mappers.StudentMapper;
import pl.cansoft.work.models.Student;
import pl.cansoft.work.services.StudentService;

import javax.validation.Valid;

@RestController
@RequestMapping("student")
@RequiredArgsConstructor
public class StudentController {

    final StudentService studentService;
    final StudentMapper studentMapper;

    /**
     *  localhost:8090?page=3&size=10&firstName=Damian&lastName=Kowalski
     *  localhost:8090 ? page=0 & size=10 & lastName=Kowalski
     *  localhost:8090 ? page=0 & size=10 // find all
     */
    @GetMapping
    Page<StudentDto> getStudents(
            @RequestParam(required = false, name = "firstName") String firstName,
            @RequestParam(required = false, name = "lastName") String lastName,
            @PageableDefault() Pageable pageable
    ) {
        return studentService.getStudents(firstName, lastName, pageable)
                .map(student -> studentMapper.modelToDto(student));
    }

    @PostMapping
    StudentDto createStudent(@RequestBody @Valid StudentDto studentDto) {
        Student student = studentMapper.dtoToModel(studentDto);
        student = studentService.saveStudent(student);
        return studentMapper.modelToDto(student);
    }

    @PutMapping("/{id}")
    StudentDto updateStudent(@PathVariable Long id, @RequestBody @Valid StudentDto studentDto) {
        Student student = studentService.getStudent(id);
        Student studentToSave = studentMapper.dtoToModel(studentDto);
        studentToSave.setId(student.getId());
        student = studentService.saveStudent(studentToSave);
        return studentMapper.modelToDto(student);
    }

    @DeleteMapping("/{id}")
    StudentDto deleteStudent(@PathVariable Long id) {
        Student student = studentService.deleteStudent(id);
        return studentMapper.modelToDto(student);
    }
}
