package pl.cansoft.work.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import pl.cansoft.work.dtos.TeacherDto;
import pl.cansoft.work.mappers.TeacherMapper;
import pl.cansoft.work.models.Teacher;
import pl.cansoft.work.services.TeacherService;

import javax.validation.Valid;

@RestController
@RequestMapping("teacher")
@RequiredArgsConstructor
public class TeacherController {

    final TeacherService teacherService;
    final TeacherMapper teacherMapper;

    @GetMapping("/student/{studentId}")
    Page<TeacherDto> getTeachersByStudentId(
            @PathVariable Long studentId,
            @PageableDefault Pageable pageable
    ) {
        return teacherService.getTeachersByStudentId(studentId, pageable)
                .map(teacher -> teacherMapper.modelToDto(teacher));
    }

    @GetMapping
    Page<TeacherDto> getTeachers(
            @RequestParam(required = false, name = "firstName") String firstName,
            @RequestParam(required = false, name = "lastName") String lastName,
            @PageableDefault() Pageable pageable
    ) {
        return teacherService.getTeachers(firstName, lastName, pageable)
                .map(teacher -> teacherMapper.modelToDto(teacher));
    }

    @PostMapping
    TeacherDto createTeacher(@RequestBody @Valid TeacherDto studentDto) {
        Teacher teacher = teacherMapper.dtoToModel(studentDto);
        teacher = teacherService.saveTeacher(teacher);
        return teacherMapper.modelToDto(teacher);
    }

    @PutMapping("/{id}")
    TeacherDto updateTeacher(@PathVariable Long id, @RequestBody @Valid TeacherDto studentDto) {
        Teacher teacher = teacherService.getTeacher(id);
        Teacher teacherToSave = teacherMapper.dtoToModel(studentDto);
        teacherToSave.setId(teacher.getId());
        teacher = teacherService.saveTeacher(teacherToSave);
        return teacherMapper.modelToDto(teacher);
    }

    @DeleteMapping("/{id}")
    TeacherDto deleteTeacher(@PathVariable Long id) {
        Teacher teacher = teacherService.deleteTeacher(id);
        return teacherMapper.modelToDto(teacher);
    }
}
