package pl.cansoft.work.mappers;

import org.springframework.stereotype.Component;
import pl.cansoft.work.dtos.TeacherDto;
import pl.cansoft.work.models.Teacher;

@Component
public class TeacherMapper {

    public Teacher dtoToModel(TeacherDto dto) {
        return Teacher.builder()
                .id(dto.getId())
                .age(dto.getAge())
                .email(dto.getEmail())
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .subject(dto.getSubject())
                .build();
    }

    public TeacherDto modelToDto(Teacher model) {
        return TeacherDto.builder()
                .id(model.getId())
                .age(model.getAge())
                .email(model.getEmail())
                .firstName(model.getFirstName())
                .lastName(model.getLastName())
                .subject(model.getSubject())
                .build();
    }
}
