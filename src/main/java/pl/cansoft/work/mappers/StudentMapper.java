package pl.cansoft.work.mappers;

import org.springframework.stereotype.Component;
import pl.cansoft.work.dtos.StudentDto;
import pl.cansoft.work.models.Student;

@Component
public class StudentMapper {

    public Student dtoToModel(StudentDto dto) {
        return Student.builder()
                .id(dto.getId())
                .age(dto.getAge())
                .email(dto.getEmail())
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .major(dto.getMajor())
                .build();
    }

    public StudentDto modelToDto(Student model) {
        return StudentDto.builder()
                .id(model.getId())
                .age(model.getAge())
                .email(model.getEmail())
                .firstName(model.getFirstName())
                .lastName(model.getLastName())
                .major(model.getMajor())
                .build();
    }
}
