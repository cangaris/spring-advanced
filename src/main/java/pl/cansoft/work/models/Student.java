package pl.cansoft.work.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder // https://projectlombok.org/features/experimental/SuperBuilder
@NoArgsConstructor
@Entity
public class Student extends Person {

    private String major;

    /**
     * https://www.baeldung.com/jpa-many-to-many
     */
    @ManyToMany
    @JoinTable(
            name = "teacher_student",
            joinColumns = @JoinColumn(name = "student_id"),
            inverseJoinColumns = @JoinColumn(name = "teacher_id")
    )
    private List<Teacher> teachers;
}
