package pl.cansoft.work.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder // https://projectlombok.org/features/experimental/SuperBuilder
@NoArgsConstructor
@Entity
public class Teacher extends Person {

    private String subject;

    /**
     * https://www.baeldung.com/jpa-many-to-many
     */
    @ManyToMany(mappedBy = "teachers")
    private List<Student> students;
}
