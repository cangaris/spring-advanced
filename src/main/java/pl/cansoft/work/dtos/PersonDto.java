package pl.cansoft.work.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@Data
@SuperBuilder
@NoArgsConstructor
public class PersonDto {
    private Long id;
    @Size(min = 3)
    private String firstName;
    private String lastName;
    @Email
    private String email;
    @Min(18)
    private Short age;
}
