package pl.cansoft.work.services;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.cansoft.work.exceptions.NotFoundException;
import pl.cansoft.work.models.Student;
import pl.cansoft.work.models.Teacher;
import pl.cansoft.work.repositories.TeacherRepository;

@Service
@RequiredArgsConstructor
public class TeacherService {

    final TeacherRepository teacherRepository;
    final StudentService studentService;

    public Page<Teacher> getTeachersByStudentId(Long studentId, Pageable pageable) {
        Student student = studentService.getStudent(studentId);
        return teacherRepository.findByTeachersByStudentId(student.getId(), pageable);
    }

    public Page<Teacher> getTeachers(String firstName, String lastName, Pageable pageable) {
        if (firstName != null) {
            return teacherRepository.findByFirstName(firstName, pageable);
        }
        if (lastName != null) {
            return teacherRepository.findByLastName(lastName, pageable);
        }
        return teacherRepository.findAll(pageable);
    }

    public Teacher saveTeacher(Teacher teacherToSave) {
        return teacherRepository.save(teacherToSave);
    }

    public Teacher getTeacher(Long id) {
        return teacherRepository.findById(id) // https://www.baeldung.com/java-optional-throw-exception
                .orElseThrow(NotFoundException::new);
    }

    public Teacher deleteTeacher(Long id) {
        Teacher teacher = teacherRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        teacherRepository.delete(teacher);
        return teacher;
    }
}
