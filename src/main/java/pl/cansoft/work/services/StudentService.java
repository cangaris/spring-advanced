package pl.cansoft.work.services;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.cansoft.work.exceptions.NotFoundException;
import pl.cansoft.work.models.Student;
import pl.cansoft.work.repositories.StudentRepository;

@Service
@RequiredArgsConstructor
public class StudentService {

    final StudentRepository studentRepository;

    /**
     * https://www.baeldung.com/spring-data-jpa-pagination-sorting
     */
    public Page<Student> getStudents(String firstName, String lastName, Pageable pageable) {
        if (firstName != null) {
            return studentRepository.findByFirstName(firstName, pageable);
        }
        if (lastName != null) {
            return studentRepository.findByLastName(lastName, pageable);
        }
        return studentRepository.findAll(pageable);
    }

    public Student saveStudent(Student student) {
        return studentRepository.save(student);
    }

    public Student getStudent(Long id) {
        return studentRepository.findById(id)
                .orElseThrow(() -> { // https://www.baeldung.com/java-optional-throw-exception
                    throw new NotFoundException();
                });
    }

    public Student deleteStudent(Long id) {
        Student student = studentRepository.findById(id)
                .orElseThrow(NotFoundException::new);
        studentRepository.delete(student);
        return student;
    }
}
