package pl.cansoft.work.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.cansoft.work.models.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {

    Page<Student> findByFirstName(String firstName, Pageable pageable);

    Page<Student> findByLastName(String lastName, Pageable pageable);
}
