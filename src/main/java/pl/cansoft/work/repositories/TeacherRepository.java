package pl.cansoft.work.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.cansoft.work.models.Teacher;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Long> {

    Page<Teacher> findByFirstName(String firstName, Pageable pageable);

    Page<Teacher> findByLastName(String lastName, Pageable pageable);

    /**
     * https://www.baeldung.com/spring-data-jpa-query
     */
    @Query(" select t from Teacher t join t.students s where s.id = :id ")
    Page<Teacher> findByTeachersByStudentId(@Param("id") Long id, Pageable pageable);
}
